from __future__ import unicode_literals

import datetime

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.text import ugettext_lazy as _

from idoklad_client.rest_client import IdokladRestClient
from idoklad_client.countries import get_country_id_from_code, COUNTRIES_CHOICES


class IdokladException(Exception):

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

    def __unicode__(self):
        return self.msg


class IdokladModel(object):

    RESOURCE_URL = None
    RESOURCE_UPDATE_URL = None
    RESOURCE_DEFAULT_URL = None

    id_rest_field = 'Id'

    fields = {}
    raw_data = {}

    is_valid_errors = None

    def __init__(self, rest_connector, load_default=True):
        self.rest_connector = rest_connector
        if load_default:
            self.init_default_fields()
        self.init_non_default_fields()

    def init_default_fields(self):
        self.raw_data = self.get_default_data_from_rest()
        self.store_raw_data(self.raw_data)

    def init_non_default_fields(self):
        for field_name, field in self.fields.items():
            if not hasattr(self, field_name):
                setattr(self, field_name, field.get_default())

    def store_raw_data(self, raw_data):
        for field_name, field in self.fields.items():
            if field.db_column in raw_data:
                setattr(self, field_name, raw_data[field.db_column])

    def update_raw_data(self):
        for key, value in self.fields.items():
            if hasattr(self, key):
                self.raw_data[value.db_column] = getattr(self, key)

    def get_default_data_from_rest(self):
        r = self.rest_connector.do_request(self.rest_connector.GET, self.RESOURCE_DEFAULT_URL)
        return r.json() if r.status_code == 200 else None

    def is_valid(self):
        for key, db_field in self.fields.items():
            try:
                db_field.clean(getattr(self, key), None)
            except ValidationError as e:
                raise IdokladException('%s: %s' % (key, e))
        return True

    def create(self):
        self.update_raw_data()
        self.is_valid()
        r = self.rest_connector.do_request(self.rest_connector.POST, self.RESOURCE_URL, data=self.raw_data)
        if r.status_code == 200:
            self.raw_data = r.json()
            self.store_raw_data(self.raw_data)
            return True
        else:
            raise IdokladException(r.text)

    def update(self):
        self.update_raw_data()
        self.is_valid()
        if not self.RESOURCE_UPDATE_URL:
            raise IdokladException('Attribute RESOURCE_UPDATE_URL is not set')

        if not getattr(self, 'pk', None):
            raise IdokladException('This object has no pk')

        url = self.RESOURCE_UPDATE_URL % {'id': self.pk}
        r = self.rest_connector.do_request(self.rest_connector.PUT, url, data=self.raw_data)
        if r.status_code in (200, 201):
            self.raw_data = r.json()
            self.store_raw_data(self.raw_data)
            return True
        else:
            raise IdokladException(r.text)

    @classmethod
    def get(cls, rest_connector, id):
        url = '%s/%s' % (cls.RESOURCE_URL, id)
        r = rest_connector.do_request(rest_connector.GET, url)
        if r.status_code == 200:
            obj = cls(rest_connector, load_default=False)
            obj.raw_data = r.json()
            obj.store_raw_data(obj.raw_data)
            return obj
        elif r.status_code == 404:
            return None
        else:
            raise IdokladException(r.text)


class Contact(IdokladModel):

    RESOURCE_DEFAULT_URL = 'api/v2/Contacts/Default'
    RESOURCE_URL = 'api/v2/Contacts'
    RESOURCE_UPDATE_URL = 'api/v2/Contacts/%(id)s'

    COUNTRY_CZECH_REPUBLIC = 2

    fields = {
        'pk': models.IntegerField(db_column='Id', null=True, blank=True),
        'company_name': models.CharField(db_column='CompanyName', max_length=200, null=False, blank=False),
        'city': models.CharField(db_column='City', max_length=50, null=True, blank=True),
        'country_id': models.IntegerField(db_column='CountryId', null=False, blank=False, choices=COUNTRIES_CHOICES),
        'email': models.EmailField(db_column='Email', null=True, blank=True, max_length=50),
        'fax': models.CharField(db_column='Fax', null=True, blank=True, max_length=20),
        'first_name': models.CharField(db_column='Firstname', null=True, blank=True, max_length=50),
        'mobile': models.CharField(db_column='Mobile', null=True, blank=True, max_length=20),
        'phone': models.CharField(db_column='Phone', null=True, blank=True, max_length=20),
        'zip_code': models.CharField(db_column='PostalCode', null=True, blank=True, max_length=11),
        'street': models.CharField(db_column='Street', null=True, blank=True, max_length=100),
        'surname': models.CharField(db_column='Surname', null=True, blank=True, max_length=50),
        'title': models.CharField(db_column='Title', null=True, blank=True, max_length=50),
        'www': models.CharField(db_column='Www', null=True, blank=True, max_length=50),
        'tin': models.CharField(db_column='IdentificationNumber', null=True, blank=True, max_length=20),
        'vatin': models.CharField(db_column='VatIdentificationNumber', null=True, blank=True, max_length=20),
    }

    def set_country(self, code):
        country_id = get_country_id_from_code(code)
        if country_id:
            self.country_id = country_id
            return True
        else:
            return False


class OutputInvoice(IdokladModel):

    RESOURCE_DEFAULT_URL = 'api/v2/IssuedInvoices/Default'
    RESOURCE_URL = 'api/v2/IssuedInvoices'

    FULLY_PAY_URL = 'api/v2/IssuedInvoices/%(id)s/FullyPay?dateOfPayment=%(payment_date)s'
    SEND_EMAIL_TO_PURCHASER_URL = 'api/v2/IssuedInvoices/%(id)s/SendMailToPurchaser'

    FIELD_ITEMS = 'IssuedInvoiceItems'

    CURRENCY_CZK = 1
    CURRENCY_EUR = 2
    CURRENCY_USD = 11

    CURRENCY_CHOICES = (
        (CURRENCY_CZK, 'CZK'),
        (CURRENCY_EUR, 'EUR'),
        (CURRENCY_USD, 'USD'),
    )

    PAYMENT_STATUS_UNPAID = 0
    PAYMENT_STATUS_PAID = 1
    PAYMENT_STATUS_PARTIAL_PAID = 2
    PAYMENT_STATUS_OVERPAID = 3

    fields = {
        'pk': models.IntegerField(db_column='Id', null=True, blank=True),
        'description': models.CharField(db_column='Description', max_length=200, null=True, blank=True),
        'order_number': models.CharField(db_column='OrderNumber', max_length=20, null=True, blank=True),
        'customer_id': models.IntegerField(db_column='PurchaserId', null=False, blank=False),
        'currency_id': models.IntegerField(db_column='CurrencyId', null=False, blank=False, choices=CURRENCY_CHOICES),
        'variable_symbol': models.CharField(db_column='VariableSymbol', max_length=10, null=False, blank=False),
        'items_text_prefix': models.TextField(db_column='ItemsTextPrefix', null=True, blank=True),
        'items_text_suffix': models.TextField(db_column='ItemsTextSuffix', null=True, blank=True),
    }

    items = None

    def clean_items(self):
        self.items = []

    def add_item(self, item):
        if not self.items:
            self.items = []
        self.items.append(item)

    def is_valid(self):
        super(OutputInvoice, self).is_valid()
        for item in self.items:
            item.is_valid()
        return True

    def items_to_dicts_in_list(self):
        return [item.serialize_to_dict() for item in self.items]

    def update_raw_data(self):
        super(OutputInvoice, self).update_raw_data()
        self.raw_data[self.FIELD_ITEMS] = self.items_to_dicts_in_list()

    def fully_pay(self, payment_date=None):
        payment_date = payment_date or datetime.date.today()
        url = self.FULLY_PAY_URL % {'id': self.pk, 'payment_date': payment_date}
        r = self.rest_connector.do_request(self.rest_connector.PUT, url)
        if r.status_code == 200:
            return r.json()
        else:
            raise IdokladException(r.text)

    def send_email_to_purchaser(self):
        url = self.SEND_EMAIL_TO_PURCHASER_URL % {'id': self.pk}
        r = self.rest_connector.do_request(self.rest_connector.PUT, url)
        if r.status_code == 200:
            return r.json()
        elif r.status_code == 404:
            return False
        else:
            raise IdokladException(r.text)

    def get_payment_status(self):
        return self.raw_data.get('PaymentStatus', None)

    def get_total(self):
        return self.raw_data.get('TotalWithVat', None)


class OutputInvoiceItem(IdokladModel):

    PRICE_WITH_VAT = 0
    PRICE_WITHOUT_VAT = 1
    PRICE_ONLY_BASE = 2

    DEFAULT_PRICE = PRICE_WITH_VAT

    PRICE_CHOICES = (
        (PRICE_WITH_VAT, _('Price with vat')),
        (PRICE_WITHOUT_VAT, _('Price without vat')),
        (PRICE_ONLY_BASE, _('Price - only base')),
    )

    VAT_REDUCED_1 = 'Reduced1'
    VAT_BASIC = 'Basic'
    VAT_ZERO = 'Zero'
    VAT_REDUCED_2 = 'Reduced2'

    DEFAULT_VAT = VAT_BASIC

    VAT_RATE_CHOICES = (
        (VAT_REDUCED_1, _('VAT - reduced 1')),
        (VAT_BASIC, _('VAT - basic')),
        (VAT_ZERO, _('VAT - zero')),
        (VAT_REDUCED_2, _('VAT - reduced 2')),
    )

    fields = {
        'amount': models.DecimalField(db_column='Amount', null=False, blank=False, decimal_places=5, max_digits=15,
                                      default=1),
        'name': models.CharField(db_column='Name', max_length=200, null=False, blank=False),
        'price_type': models.IntegerField(db_column='PriceType', null=False, blank=False, choices=PRICE_CHOICES,
                                          default=DEFAULT_PRICE),
        'unit_price': models.DecimalField(db_column='UnitPrice', null=False, blank=False, decimal_places=4,
                                          max_digits=15, default=0),
        'vat_rate_type': models.CharField(db_column='VatRateType', max_length=10, null=False, blank=False,
                                          choices=VAT_RATE_CHOICES, default=DEFAULT_VAT),
        'unit': models.CharField(db_column='Unit', max_length=20, null=True, blank=True),
    }

    def __init__(self, name, unit_price, amount, price_type=None, vat_rate_type=None, unit=None):
        self.name = name
        self.unit_price = unit_price
        self.amount = amount
        self.unit = unit
        if not price_type is None:
            self.price_type = price_type
        if not vat_rate_type is None:
            self.vat_rate_type = vat_rate_type
        self.init_non_default_fields()

    def serialize_to_dict(self):
        return dict((value.db_column, getattr(self, key, None)) for key, value in self.fields.items())

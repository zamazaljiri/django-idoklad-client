from __future__ import unicode_literals

import json
import requests
import decimal


def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return str(obj)
    raise TypeError


class IdokladRestClient(object):

    BASE_ENDPOINT = 'https://app.idoklad.cz/developer/'
    DEFAULT_HEADERS = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    GET_ACCESS_TOKEN_URL = 'https://app.idoklad.cz/identity/server/connect/token'

    GET = 'get'
    POST = 'post'
    PUT = 'put'

    def __init__(self, client_id, client_secret, app_name=None, app_version=None):
        self.client_id = client_id
        self.client_secret = client_secret
        self.app_name = app_name
        self.app_version = app_version
        self.access_token = None

    def _serialize(self, data):
        return json.dumps(data, default=decimal_default)

    def reset(self):
        self.db_name = None

    def get_headers(self):
        headers = self.DEFAULT_HEADERS.copy()
        if self.app_name:
            headers['X-App'] = self.app_name
        if self.app_version:
            headers['X-App-Version'] = self.app_version
        if self.access_token:
            headers['Authorization'] = 'Bearer {}'.format(self.access_token)
        return headers

    def do_request(self, method, url, data=None, params=None, headers=None, append_base_endpoint=True):
        if append_base_endpoint:
            url = self.BASE_ENDPOINT + url
        if not headers:
            headers = self.get_headers()
        if data and isinstance(data, dict):
            data = self._serialize(data)
        return getattr(requests, method)(url, data=data, params=params, headers=headers)

    def _get_access_token(self, client_id, client_secret):
        params = {'scope': 'idoklad_api', 'grant_type': 'client_credentials', 'client_id': client_id,
                  'client_secret': client_secret}
        r = requests.post(self.GET_ACCESS_TOKEN_URL, data=params)
        return r.json().get('access_token') if r.status_code == 200 else None

    def login(self):
        self.access_token = self._get_access_token(self.client_id, self.client_secret)
        return bool(self.access_token)

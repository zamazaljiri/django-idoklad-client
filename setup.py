from setuptools import setup, find_packages

from idoklad_client.version import get_version

setup(
    name='django-idoklad-client',
    version=get_version(),
    description="A pluggable Django application for integrating iDoklad (idoklad.cz)",
    keywords='django, admin, payment, idoklad',
    author='Zamazal Jiri',
    author_email='zamazal.jiri@gmail.com',
    url='https://bitbucket.org/zamazaljiri/django-idoklad-client',
    license='LGPL',
    package_dir={'idoklad_client': 'idoklad_client'},
    include_package_data=True,
    packages=find_packages(),
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU LESSER GENERAL PUBLIC LICENSE (LGPL)',
        'Natural Language :: Czech',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Internet :: WWW/HTTP :: Site Management',
    ],
    install_requires=[
        'django>=1.5',
        'requests==2.4.3'
    ],
    zip_safe=False
)

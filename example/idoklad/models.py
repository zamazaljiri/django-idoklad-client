from __future__ import unicode_literals

from idoklad_client import rest_models as idoklad_models


class Contact(idoklad_models.Contact):
    pass


class OutputInvoice(idoklad_models.OutputInvoice):
    pass


class OutputInvoiceItem(idoklad_models.OutputInvoiceItem):
    pass

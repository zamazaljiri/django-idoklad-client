# -*- coding: utf8 -*-

from __future__ import unicode_literals

import datetime

from django.core.management.base import BaseCommand, CommandError

from idoklad.models import Contact, OutputInvoice, OutputInvoiceItem
from idoklad_client.rest_client import IdokladRestClient


class Command(BaseCommand):
    help = 'Testik'

    def handle(self, *args, **options):
        print "Testik....."

        rest_client = IdokladRestClient(client_id="ca768cad-57e7-4daf-bd52-c0d0c4c2a13a", client_secret="5c73d29b-3361-4006-8aa3-0349c0768b46")
        rest_client.login()

        contact = Contact(rest_client)
        contact.first_name = 'Jiri AAAA'
        contact.last_name = 'Novak AAA'
        contact.company_name = 'Jiri Novak (123) Bleee'
        print contact.set_country('cz')
        contact.tin = '777888999555'
        contact.vatin = 'CZ 777888999555'
        contact.phone = 'Pevny telefon 123456'
        contact.mobile = 'Mobilni telefon 1234'
        contact.create()

        invoice = OutputInvoice(rest_client)
        invoice.customer_id = contact.pk
        invoice.order_number = 'OBJ 445566'
        invoice.description = 'Clenstvi v databazi XXXYYY'
        invoice.currency_id = OutputInvoice.CURRENCY_USD
        invoice.items_text_prefix = ''
        invoice.items_text_suffix = ''

        invoice.clean_items()
        invoice.add_item(OutputInvoiceItem(name='Polozka s DPH', unit_price=100, amount=2))
        invoice.add_item(OutputInvoiceItem(name='Polozka bez DPH', unit_price=100, amount=2,
                                           price_type=OutputInvoiceItem.PRICE_WITHOUT_VAT, unit='kg'))
        invoice.add_item(OutputInvoiceItem(name='Polozka zaklad', unit_price=100, amount=2,
                                           price_type=OutputInvoiceItem.PRICE_ONLY_BASE, unit='kg'))
        invoice.add_item(OutputInvoiceItem(name='Polozka bez DPH snizena', unit_price=100, amount=2,
                                           price_type=OutputInvoiceItem.PRICE_WITHOUT_VAT,
                                           vat_rate_type=OutputInvoiceItem.VAT_REDUCED_1, unit='řeřicha česž'))

        invoice.create()

        # invoice = OutputInvoice.get(rest_client, 1917544)
        #
        # print invoice
        # result = invoice.send_email_to_purchaser()
        # print result
        # print type(result)


        # contact = Contact.get(rest_client, 1201521)
        # contact.company_name = 'UPDATE NAME'
        # contact.city = "Praha 777"
        # contact.update()


        # contact = Contact(rest_client)
        # contact.first_name = 'Jiri AAAA'
        # contact.last_name = 'Novak AAA'
        # contact.company_name = 'Jiri Novak (123) Bleee'
        # print contact.set_country('cz')
        # contact.tin = '777888999555'
        # contact.vatin = 'CZ 777888999555'
        # contact.phone = 'Pevny telefon 123456'
        # contact.mobile = 'Mobilni telefon 1234'
        # contact.create()
        #
        # invoice = OutputInvoice(rest_client)
        # invoice.customer_id = contact.pk
        # invoice.order_number = 'OBJ 445566'
        # invoice.description = 'Clenstvi v databazi XXXYYY'
        # invoice.currency_id = OutputInvoice.CURRENCY_USD
        # invoice.items_text_prefix = ''
        # invoice.items_text_suffix = ''
        #
        # invoice.clean_items()
        # invoice.add_item(OutputInvoiceItem(name='Polozka s DPH', unit_price=100, amount=2))
        # invoice.add_item(OutputInvoiceItem(name='Polozka bez DPH', unit_price=100, amount=2,
        #                                    price_type=OutputInvoiceItem.PRICE_WITHOUT_VAT))
        # invoice.add_item(OutputInvoiceItem(name='Polozka zaklad', unit_price=100, amount=2,
        #                                    price_type=OutputInvoiceItem.PRICE_ONLY_BASE))
        # invoice.add_item(OutputInvoiceItem(name='Polozka bez DPH snizena', unit_price=100, amount=2,
        #                                    price_type=OutputInvoiceItem.PRICE_WITHOUT_VAT,
        #                                    vat_rate_type=OutputInvoiceItem.VAT_REDUCED_1))
        #
        # invoice.create()

        # print type(invoice.fully_pay(datetime.date(2014, 5, 20)))
        #
        # invoice = OutputInvoice.get(rest_client, 2009783)
        #
        # print invoice.pk
        # print invoice.get_payment_status()
        # print invoice.get_total()
        #
        # print OutputInvoice.get(rest_client, 1917544)
        # print OutputInvoice.get(rest_client, 2222222)

        print "Done...."
